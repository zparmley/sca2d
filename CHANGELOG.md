# Changelog

## v0.2.0

* Envionment variable OPENSCADPATH is now checked when defining path for include/use
* Comparison of called arguments to defined arguments
* Allow redefinition of a keyword argument inside module definition scope, if default variable is undef
* Greater testing
* Fix bug when detecting pointless termination or pointless scope
* Fix bug where keyword arguments in function expressions are interpreted as used variables
* Fix typo in message

## v0.1.2

* Fix parser error on empty file
* Fix errors when calculating whitespace postion at start or end of file

## v0.1.1

* Fix parsing of let expression that returns a list
* Fix excluding trees when calculating complexity

## v0.1.0

* Warn for unneeded "use" and "include" statements
* Implement OpenSCAD 2021 syntax (Function literals, caret symbol power)
* Implement trailing whitespace checks
* Warn for use of implicit module scopes
* Find empty arguments or empty items in lists
* Improved (but still poor) complexity metric
* More self testing

## v0.0.8

* Disable variable overwritten within scope warning for special variables (requires further thought)
* Add gitlab code quality output

## v0.0.7

* Stop SCAD2D crashing if an argument is empty
* Disable certain warnings for special variables
* Add output summary
* Improve exit code

## v0.0.6

* Can analyse a whole folder from the command line (considerably faster than a bash loop) as files are only lexed and parsed once
* Analysis of list comprehensions
* Fixed bugs in handling of complex nested functions like let and assert
* Added method to execute SCAD2D on code from within python
* Added unit tests for the analyser (more needed)
* Added way to disable some warnings from within python
* Stopped certain warnings in the wrong context (such as unused function in outer scope of a file)

## v0.0.5

* Big improvement to file includes and use
* Messaging system is now more unified
* Can lex and parse (but not analyse) list comprehensions
* Attribute style indexing (e.g. `list.x`) now works
* Modifier characters now are understood
* Function-style echo and assert statements are now supported
* Updated allowed variable names
* Moved printing what files is being parsed into a `--verbose` mode

## v0.0.4

* Fixed bug where function calls were not added to used calls
* Separated out messages definition from main parsing code
* Recategorised messages
* Assign statements now work
* Let expressions (i.e. when used in an assignment) now work
* Added option to colourise the output

## v0.0.3

Fixed including the lark file in the wheel.

## v0.0.2

Improved file handling for include/use.

## v0.0.1

Basic analysis. Can parse most (all?) scad files. Messaging and file import rather *ad-hoc*/
